package pl.adamchodera.sdabazydanych;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

public class UsersAdapter extends ArrayAdapter<String> {

    public UsersAdapter(@NonNull final Context context, @LayoutRes final int resource) {
        super(context, resource);
    }
}
