package pl.adamchodera.sdabazydanych;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.adamchodera.sdabazydanych.data.DaoSession;
import pl.adamchodera.sdabazydanych.data.Task;
import pl.adamchodera.sdabazydanych.data.TaskDao;
import pl.adamchodera.sdabazydanych.data.User;
import pl.adamchodera.sdabazydanych.data.UserDao;

public class TasksListActivity extends AppCompatActivity implements TasksListAdapter.TaskCompleteListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private TasksListAdapter tasksListAdapter;
    private LoadTasksFromDatabaseAsyncTask loadTasksFromDatabaseAsyncTask;
    private SetTaskAsCompletedAsyncTask setTaskAsCompletedAsyncTask;
    private TaskDao taskDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_list);
        ButterKnife.bind(this);


        MyApplication myApplication = ((MyApplication) getApplication());
        final DaoSession daoSession = myApplication.getDaoSession();
        taskDao = daoSession.getTaskDao();

        // generate two users
//        final User user = new User();
//        user.setName("first user");
//        final User user2 = new User();
//        user2.setName("second user");
//
//        final UserDao userDao = daoSession.getUserDao();
//        userDao.save(user);
//        userDao.save(user2);

        initTaskList();
    }


    private void printAllUsersAndTheirsTasks() {
        final DaoSession daoSession = ((MyApplication) getApplication()).getDaoSession();
        final UserDao userDao = daoSession.getUserDao();

        final List<User> usersList = userDao.queryBuilder().list();

        for (Iterator<User> userIterator = usersList.iterator(); userIterator.hasNext(); ) {
            final User user = userIterator.next();
            final List<Task> userTasks = user.getTasks();

            for (Iterator<Task> userTasksIterator = userTasks.iterator(); userTasksIterator.hasNext(); ) {
                final Task task = userTasksIterator.next();

                Log.e("TasksListActivity", user.getName() + ": " + task.getTitle());
            }
        }
    }

    private void initTaskList() {
        tasksListAdapter = new TasksListAdapter(this);
        recyclerView.setAdapter(tasksListAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadTasksFromDatabaseAsyncTask = new LoadTasksFromDatabaseAsyncTask();
        loadTasksFromDatabaseAsyncTask.execute();

        printAllUsersAndTheirsTasks();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (setTaskAsCompletedAsyncTask != null) {
            setTaskAsCompletedAsyncTask.cancel(true);
        }
        loadTasksFromDatabaseAsyncTask.cancel(true);
    }

    @OnClick(R.id.add_task_button)
    public void goToAddNoteActivity() {
        Intent intent = new Intent(this, TaskDetailsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTaskCompleted(final Task task) {
        setTaskAsCompletedAsyncTask = new SetTaskAsCompletedAsyncTask(task);
        setTaskAsCompletedAsyncTask.execute();
    }

    private class LoadTasksFromDatabaseAsyncTask extends AsyncTask<Void, Void, List<Task>> {

        @Override
        protected List<Task> doInBackground(Void... params) {

            QueryBuilder<Task> qb = taskDao.queryBuilder();
            qb.where(TaskDao.Properties.IsCompleted.eq(false));

            List<Task> notCompletedTasks = qb.list();

            return notCompletedTasks;
        }

        @Override
        protected void onPostExecute(final List<Task> tasks) {
            super.onPostExecute(tasks);

            tasksListAdapter.setTasks(tasks);
            tasksListAdapter.notifyDataSetChanged();
        }
    }

    private class SetTaskAsCompletedAsyncTask extends AsyncTask<String, Void, List<Task>> {

        private final Task task;

        public SetTaskAsCompletedAsyncTask(Task task) {
            this.task = task;
        }

        @Override
        protected List<Task> doInBackground(String... params) {
            task.setIsCompleted(true);

            taskDao.update(task);

            QueryBuilder<Task> qb = taskDao.queryBuilder();
            qb.where(TaskDao.Properties.IsCompleted.eq(false));

            List<Task> notCompletedTasks = qb.list();

            return notCompletedTasks;
        }

        @Override
        protected void onPostExecute(final List<Task> tasks) {
            super.onPostExecute(tasks);

            tasksListAdapter.setTasks(tasks);
            tasksListAdapter.notifyDataSetChanged();
        }
    }
}
